using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using AspNetCoreTodo.Data;
using AspNetCoreTodo.Models;
using AspNetCoreTodo.Services;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreTodo.Tests
{
    public class TodoItemServiceShould
    {
        // Note: Accoding to Microsoft docs, a new in memory Db should be used for each test.
        // That means that Setup and teardown methods become relatively redundant and
        //     that we should be initializing the database each time. 
        
        
        // fake data
        private readonly ApplicationUser _fakeUser00 = new ApplicationUser
        {
            Id = "fake-000",
            UserName = "fake00@example.com"
        };  
        
        private readonly ApplicationUser _fakeUser01 = new ApplicationUser
        {
            Id = "fake-001",
            UserName = "fake01@example.com"
        };
        
        // initialization helper funciton
        private async Task<DbContextOptions<ApplicationDbContext>> InitializeDatabase(string name)
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: name).Options;
            
            using (var context = new ApplicationDbContext(options))
            {
                var service = new TodoItemService(context);

                await service.AddItemAsync(new TodoItem
                {
                    Title = "Testing"
                }, _fakeUser00);
            }

            return options;
        }
        
        [Test]
        public async Task AddNewItemAsyncShould()
        {
            // set up database in its own context instance:
            var options = await InitializeDatabase("AddNewItemAsyncShould");
            
            // run test in new context instance
            using (var context = new ApplicationDbContext(options))
            {
                var itemsInDatabase = await context.TodoItems.CountAsync();
                Assert.AreEqual(1, itemsInDatabase);

                var item = await context.TodoItems.FirstAsync();
                Assert.AreEqual("Testing", item.Title);
                Assert.False(item.IsDone);
                Assert.Null(item.DueAt);
            }
        }
        
        [Test]
        public async Task GetIncompleteItemsAsyncShould()
        {
            // set up database in its own context instance:
            var options = await InitializeDatabase("GetIncompleteItemsAsyncShould");
            
            // run test in new context instance
            using (var context = new ApplicationDbContext(options))
            {
                var service = new TodoItemService(context); 
                
                // sanity check
                var itemsInDatabase = await context.TodoItems.CountAsync();
                Assert.AreEqual(1, itemsInDatabase);
                   
                // Test
                var items = await service.GetIncompleteItemsAsync(_fakeUser00);
                var items2 = await service.GetIncompleteItemsAsync(_fakeUser01);
                
                foreach (var item in items)
                {
                    Assert.AreEqual("fake-000", item.UserId);
                }
                
                Assert.IsEmpty(items2);
            }
        }

        [Test]
        public async Task TestMarkAsDoneShould()
        {
            // set up database in its own context instance:
            var options = await InitializeDatabase("TestMarkAsDoneShould");
            
            // run test in new context instance
            using (var context = new ApplicationDbContext(options))
            {
                var itemsInDatabase = await context.TodoItems.CountAsync();
                Assert.AreEqual(1, itemsInDatabase);
                
                var service = new TodoItemService(context);

                var item = context.TodoItems
                    .First(x => x.UserId == "fake-000");

                var incorrectId = Guid.NewGuid();
                
                Assert.False(await service.MarkDoneAsync(incorrectId, _fakeUser00));
                Assert.False(await service.MarkDoneAsync(item.Id, _fakeUser01));
                Assert.True(await service.MarkDoneAsync(item.Id, _fakeUser00));
                item.IsDone = false; //reset
            }
        }
    }
}