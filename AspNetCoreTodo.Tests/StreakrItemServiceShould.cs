﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using AspNetCoreTodo.Data;
using AspNetCoreTodo.Models;
using AspNetCoreTodo.Services;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreTodo.Tests
{
    public class StreakrItemServiceShould
    {
        // fake data
        private readonly ApplicationUser _fakeUser00 = new ApplicationUser
        {
            Id = "fake-000",
            UserName = "fake00@example.com"
        };  
        
        private readonly StreakrItem _testItem1 = new StreakrItem
        {
            CurrentStreak = 10,
            DaysBeforeStreakEnds = 1,
            DoneToday = false,
            HighestStreak = 10,
            Id = Guid.NewGuid(),
            ItemName = "Test Streakr",
            StreakDay = DateTime.Today,
            UserId = "fake-000"
        };
        
        private readonly StreakrItem _testItem2 = new StreakrItem
        {
            CurrentStreak = 10,
            DaysBeforeStreakEnds = 1,
            DoneToday = false,
            HighestStreak = 10,
            Id = Guid.NewGuid(),
            ItemName = "Test Streakr",
            StreakDay = DateTime.Today.AddDays(-2),
            UserId = "fake-000"
        };
        
        private async Task<DbContextOptions<ApplicationDbContext>> InitializeDatabase(string name)
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: name).Options;
            
            return options;
        }

        [Test]
        public async Task TestAddItemAsync()
        {
            var options = await InitializeDatabase("TestAddItemAsync");
            using (var context = new ApplicationDbContext(options))
            {
                var service = new StreakrItemService(context);
                
                var itemToAdd = new StreakrItem
                {
                    ItemName = "New Item",
                    DaysBeforeStreakEnds = 9
                };

                await service.AddItemAsync(itemToAdd, _fakeUser00);
            }

            using (var context = new ApplicationDbContext(options))
            {
                // sanity check
                var itemsCount = await context.StreakrItems.CountAsync();
                Assert.AreEqual(1, itemsCount);

                var item = await context.StreakrItems.FirstOrDefaultAsync();
                Assert.AreEqual("New Item", item.ItemName);
                Assert.IsInstanceOf<Guid>(item.Id);
                Assert.AreEqual(0, item.HighestStreak);
                Assert.AreEqual(0, item.CurrentStreak);
                Assert.AreEqual(DateTime.Today, item.StreakDay);
                Assert.AreEqual(1, item.DaysBeforeStreakEnds);
                Assert.IsFalse(item.DoneToday);
            }
        }

        [Test]
        public async Task TestCompleteItemAsync()
        {
            var options = await InitializeDatabase("TestCompleteItemAsync");
            using (var context = new ApplicationDbContext(options))
            {
                context.StreakrItems.Add(_testItem1);
                context.StreakrItems.Add(_testItem2);
                await context.SaveChangesAsync();
            }
            
            using (var context = new ApplicationDbContext(options))
            {
                // sanity check
                var itemsCount = await context.StreakrItems.CountAsync();
                Assert.AreEqual(2, itemsCount);

                var service = new StreakrItemService(context);

                await service.CompleteItemAsync(_testItem1.Id, _fakeUser00);
                await service.CompleteItemAsync(_testItem2.Id, _fakeUser00);

                var item1 = await context.StreakrItems
                    .Where(x => x.Id == _testItem1.Id)
                    .SingleOrDefaultAsync();

                Assert.AreEqual(11, item1.CurrentStreak);
                Assert.AreEqual(11, item1.HighestStreak);
                Assert.AreEqual(DateTime.Today, item1.StreakDay);
                Assert.IsTrue(item1.DoneToday);

                var item2 = await context.StreakrItems
                    .Where(x => x.Id == _testItem2.Id)
                    .SingleOrDefaultAsync();
                
                Assert.AreEqual(1, item2.CurrentStreak);
                Assert.AreEqual(10, item2.HighestStreak);
                Assert.AreEqual(DateTime.Today, item2.StreakDay);
                Assert.IsTrue(item2.DoneToday);
            }
        }

        [Test]
        public async Task TestGetStreakrItems()
        {
            var options = await InitializeDatabase("TestGetStreakrItems");
            using (var context = new ApplicationDbContext(options))
            {
                context.StreakrItems.Add(_testItem1);
                context.StreakrItems.Add(_testItem2);
                await context.SaveChangesAsync();
            }

            using (var context = new ApplicationDbContext(options))
            {
                // sanity check
                var itemsCount = await context.StreakrItems.CountAsync();
                Assert.AreEqual(2, itemsCount);

                var service = new StreakrItemService(context);

                var items = await service.GetStreakrItems(_fakeUser00);
                
                Assert.AreEqual(10, items[0].CurrentStreak);
                Assert.AreEqual(0, items[1].CurrentStreak);
                Assert.AreEqual(DateTime.Today, items[0].StreakDay);
                Assert.AreEqual(DateTime.Today, items[1].StreakDay);
            }
        }
        
    }
}