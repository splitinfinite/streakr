﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreTodo.Data;
using AspNetCoreTodo.Models;
using AspNetCoreTodo.Services;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AspNetCoreTodo.UnitTests
{
    public class TodoItemServiceShould
    {
        
        private readonly DbContextOptions<ApplicationDbContext> _options = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseInMemoryDatabase(databaseName: "TestDb").Options;

        private bool _seeded;
        
        // fake data
        private readonly ApplicationUser _fakeUser00 = new ApplicationUser
        {
            Id = "fake-000",
            UserName = "fake00@example.com"
        };  
        
        private readonly ApplicationUser _fakeUser01 = new ApplicationUser
        {
            Id = "fake-001",
            UserName = "fake01@example.com"
        };
        
        [Fact]
        public async Task AddNewItemAsIncomplete()
        {
            Assert.False(_seeded);
            SeedDatabase();
            
            Assert.True(_seeded);
            
            using (var context = new ApplicationDbContext(_options))
            {
                var itemsInDatabase = await context.Items.CountAsync();
                // Assert.Equal(1, itemsInDatabase);

                var item = await context.Items.FirstAsync();
                Assert.Equal("Testing?", item.Title);
                Assert.False(item.IsDone);
                Assert.Null(item.DueAt);
            }
        }

        [Fact]
        public async Task TestGetIncompleteItemsAsync()
        {
            Assert.False(_seeded);
            SeedDatabase();

            using (var context = new ApplicationDbContext(_options))
            {
                var service = new TodoItemService(context); 
                   
                // Test
                var items = await service.GetIncompleteItemsAsync(_fakeUser00);
                var items2 = await service.GetIncompleteItemsAsync(_fakeUser01);
                
                foreach (var item in items)
                {
                    Assert.Equal("fake-000", item.UserId);
                }
                
                Assert.Empty(items2);
            }
        }

        [Fact]
        public async Task TestMarkAsDone()
        {
            Assert.False(_seeded);
            SeedDatabase();
            
            using (var context = new ApplicationDbContext(_options))
            {
                var service = new TodoItemService(context);

                var item = context.Items
                    .First(x => x.UserId == "fake-000");

                var incorrectId = Guid.NewGuid();
                
                Assert.False(await service.MarkDoneAsync(incorrectId, _fakeUser00));
                Assert.False(await service.MarkDoneAsync(item.Id, _fakeUser01));
                Assert.True(await service.MarkDoneAsync(item.Id, _fakeUser00));
                item.IsDone = false; //reset
            }
        }


        // Helper methods
        private async void SeedDatabase()
        {
            if (_seeded)
                return;
            
            using (var context = new ApplicationDbContext(_options))
            {
                var service = new TodoItemService(context);             

                await service.AddItemAsync(new TodoItem
                {
                    Title = "Testing?"
                }, _fakeUser00);
            }

            _seeded = true;
        }
    }
}