﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AspNetCoreTodo.Data.Migrations
{
    public partial class StreakrItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.DropPrimaryKey(
//                name: "PK_Items",
//                table: "Items");

            migrationBuilder.RenameTable(
                name: "Items",
                newName: "TodoItems");

//            migrationBuilder.AddPrimaryKey(
//                name: "PK_TodoItems",
//                table: "TodoItems",
//                column: "Id");

            migrationBuilder.CreateTable(
                name: "StreakrItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CurrentStreak = table.Column<int>(nullable: false),
                    DaysBeforeStreakEnds = table.Column<int>(nullable: false),
                    DoneToday = table.Column<bool>(nullable: false),
                    HighestStreak = table.Column<int>(nullable: false),
                    ItemName = table.Column<string>(nullable: false),
                    StreakDay = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StreakrItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StreakrItems");

//            migrationBuilder.DropPrimaryKey(
//                name: "PK_TodoItems",
//                table: "TodoItems");

            migrationBuilder.RenameTable(
                name: "TodoItems",
                newName: "Items");

//            migrationBuilder.AddPrimaryKey(
//                name: "PK_Items",
//                table: "Items",
//                column: "Id");
        }
    }
}
