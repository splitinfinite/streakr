﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AspNetCoreTodo.Models
{
    public class StreakrItem
    {
        public string UserId { get; set; }
        public Guid Id { get; set; }
        public bool DoneToday { get; set; }
        [Required] public string ItemName { get; set; }
        public DateTime StreakDay { get; set; }
        public int DaysBeforeStreakEnds { get; set; }
        public int CurrentStreak { get; set; }
        public int HighestStreak { get; set; }
    }
}