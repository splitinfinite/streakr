﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreTodo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreTodo.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ManageUsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public ManageUsersController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var admins = (await _userManager
                    .GetUsersInRoleAsync("Administrator"))
                .ToArray();
            var everyone = await _userManager.Users
                .ToArrayAsync();

            var model = new ManageUsersViewModel
            {
                Administrators = admins,
                Everyone = everyone
            };

            return View(model);
        }

        public async Task<IActionResult> RemoveUser(string id)
        {
            if (id == null) return RedirectToAction("Index");
            var userTodelete = await _userManager.FindByIdAsync(id);
          
            var successful = await _userManager.DeleteAsync(userTodelete);
            if (successful.Succeeded == false) return BadRequest("Could not delete user.");

            return RedirectToAction("Index");
        }
    }
}