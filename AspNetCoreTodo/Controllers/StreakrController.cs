﻿using System;
using System.Threading.Tasks;
using AspNetCoreTodo.Models;
using AspNetCoreTodo.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreTodo.Controllers
{
    [Authorize]
    public class StreakrController : Controller
    {
        private readonly IStreakrItemService _streakrItemService;
        private readonly UserManager<ApplicationUser> _userManager;

        public StreakrController(IStreakrItemService streakrItemService, UserManager<ApplicationUser> userManager)
        {
            _streakrItemService = streakrItemService;
            _userManager = userManager;
        }

        
        public async Task<IActionResult> Index()
        {
            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser == null)
                return Challenge();

            var items = await _streakrItemService.GetStreakrItems(currentUser);
          
            var model = new StreakrViewModel{StreakrItems = items};

            return View(model);
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddItem(StreakrItem newItem)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Index");

            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser == null)
                return Challenge();

            var successful = await _streakrItemService.AddItemAsync(newItem, currentUser);
            if (!successful)
                return BadRequest("Could not add new streak");

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MarkCompleted(Guid id)
        {
            if (id == Guid.Empty)
                return RedirectToAction("Index");

            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser == null)
                return Challenge();

            var successful = await _streakrItemService.CompleteItemAsync(id, currentUser);
            if (!successful)
                return BadRequest("Could not mark streak as completed");

            return RedirectToAction("Index");
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveItem(Guid id)
        {
            if (id == Guid.Empty)
                return RedirectToAction("Index");

            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser == null)
                return Challenge();

            var successful = await _streakrItemService.RemoveItemAsync(id, currentUser);
            if (!successful)
                return BadRequest("Could not remove streak");

            return RedirectToAction("Index");
        }
    }
}