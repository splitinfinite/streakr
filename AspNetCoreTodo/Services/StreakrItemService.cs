﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreTodo.Data;
using AspNetCoreTodo.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreTodo.Services
{
    public class StreakrItemService : IStreakrItemService
    {
        private readonly ApplicationDbContext _context;

        public StreakrItemService(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public async Task<StreakrItem[]> GetStreakrItems(ApplicationUser user)
        {
            var items = await _context.StreakrItems
                .Where(x => x.UserId == user.Id)
                .ToArrayAsync();
            
            // sanitise based on date:           
            foreach (var item in items)
            {
                if (item.StreakDay != DateTime.Today && item.DoneToday)
                {
                    item.DoneToday = false;
                }
                
                if (item.StreakDay.AddDays(item.DaysBeforeStreakEnds) < DateTime.Today)
                {
                    item.StreakDay = DateTime.Today;
                    item.CurrentStreak = 0;
                }
            }

            await _context.SaveChangesAsync();
            
            return items;
        }

        public async Task<bool> AddItemAsync(StreakrItem newItem, ApplicationUser user)
        {
            newItem.UserId = user.Id;
            newItem.Id = Guid.NewGuid();
            newItem.DoneToday = false;
            newItem.HighestStreak = 0;
            newItem.CurrentStreak = 0;
            newItem.StreakDay = DateTime.Today;
            if (newItem.DaysBeforeStreakEnds < 1 || newItem.DaysBeforeStreakEnds > 5)
                newItem.DaysBeforeStreakEnds = 1;

            _context.StreakrItems.Add(newItem);

            return await UpdateDatabase();
        }

        public async Task<bool> RemoveItemAsync(Guid id, ApplicationUser currentUser)
        {
            var item = await _context.StreakrItems
                .Where(x => x.Id == id && x.UserId == currentUser.Id)
                .SingleOrDefaultAsync();

            if (item == null)
                return false;

            _context.StreakrItems.Remove(item);

            return await UpdateDatabase();
        }

        public async Task<bool> CompleteItemAsync(Guid id, ApplicationUser currentUser)
        {
            var item = await _context.StreakrItems
                .Where(x => x.Id == id && x.UserId == currentUser.Id)
                .SingleOrDefaultAsync();

            if (item.StreakDay.AddDays(item.DaysBeforeStreakEnds) < DateTime.Today)
            {
                item.CurrentStreak = 0;
            }

            if (item.DoneToday == false)
            {
                item.CurrentStreak += 1;
                if (item.CurrentStreak > item.HighestStreak)
                    item.HighestStreak = item.CurrentStreak;
                item.StreakDay = DateTime.Today;
                item.DoneToday = true;   
            }
            

            return await UpdateDatabase();
        }

        
        
        
        private async Task<bool> UpdateDatabase()
        {
            return await _context.SaveChangesAsync() == 1;
        }
        
        private async Task<StreakrItem> GetItemById(ApplicationUser currentUser, Guid id)
        {
            var item = await _context.StreakrItems
                .Where(x => x.Id == id && x.UserId == currentUser.Id)
                .SingleOrDefaultAsync();
            return item;
        }
        
        
        // Unneeded methods
        
        
//        public async Task<bool> MarkCompletedAsync(Guid id, ApplicationUser currentUser)
//        {
//            var item = await _context.StreakrItems
//                .Where(x => x.Id == id && x.UserId == currentUser.Id)
//                .SingleOrDefaultAsync();
//
//            if (item == null)
//                return false;
//
//            item.DoneToday = true;
//
//            return await UpdateDatabase();
//        }
        
//        public async Task<bool> UpdateItemAsync(ApplicationUser currentUser, StreakrItem updatedItem)
//        {
//            var item = await GetItemById(currentUser, updatedItem.Id);
//
//            if (item == null)
//                return false;
//            
//            // update with new data
//            item = updatedItem;
//
//            return await UpdateDatabase();
//        }
    }
}