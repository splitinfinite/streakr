﻿using System;
using System.Threading.Tasks;
using AspNetCoreTodo.Models;

namespace AspNetCoreTodo.Services
{
    public interface IStreakrItemService
    {
        Task<StreakrItem[]> GetStreakrItems(ApplicationUser user);
        Task<bool> AddItemAsync(StreakrItem newItem, ApplicationUser user);
        Task<bool> RemoveItemAsync(Guid id, ApplicationUser currentUser);
        Task<bool> CompleteItemAsync(Guid id, ApplicationUser currentUser);
    }
}