﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AspNetCoreTodo.Data;
using AspNetCoreTodo.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AspNetCoreTodo.Services
{
    public class TodoItemService : ITodoItemService
    {
        private readonly ApplicationDbContext _context;

        public TodoItemService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<TodoItem[]> GetIncompleteItemsAsync(ApplicationUser user)
        {
            var items = await _context.TodoItems
                .Where(x => x.IsDone == false && x.UserId == user.Id)
                .ToArrayAsync();
            return items;
        }

        public async Task<bool> AddItemAsync(TodoItem newItem, ApplicationUser currentUser)
        {
            newItem.UserId = currentUser.Id;
            newItem.Id = Guid.NewGuid();
            newItem.IsDone = false;
            // newItem.DueAt = DateTimeOffset.Now.AddDays(3);

            _context.TodoItems.Add(newItem);

            return await _context.SaveChangesAsync() == 1;
        }

        public async Task<bool> MarkDoneAsync(Guid id, ApplicationUser currentUser)
        {
            var item = await _context.TodoItems
                .Where(x => x.Id == id && x.UserId == currentUser.Id)
                .SingleOrDefaultAsync();

            if (item == null) return false;
            
            item.IsDone = true;

            return await _context.SaveChangesAsync() == 1;

        }
    }
}